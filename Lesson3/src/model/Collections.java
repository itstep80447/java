package model;

import java.util.*;
import java.util.stream.Collectors;

public class Collections {
    public Collection<String> collection;
    public Queue<String> queue;
    public Deque<String> deque;
    public List<String> list;
    public Set<String> set;
    public SortedSet<String> sortedSet;
    public NavigableSet<String> navigableSet;


    public Map<String, Bank> map;
    public SortedMap<String, Bank> sortedMap;
    public NavigableMap<String, Bank> navigableMap;

    public void filter(){
        List<String> colors = new ArrayList<>();
        colors.add("red");
        colors.add("green");
        colors.add("blue");
        colors.add("yellow");
        colors.add("black");
        colors = colors.stream().filter(c -> c.equals("red") || c.equals("green")).collect(Collectors.toList());
        colors = colors.stream().map(c -> c.toUpperCase()).collect(Collectors.toList());
        colors = colors.stream().filter(c -> c.length() > 3).collect(Collectors.toList());
        colors = colors.stream().sorted().collect(Collectors.toList());
    }

    public void queues(){
        Queue queue = new PriorityQueue();
        queue.add("red");
        queue.add("green");
        queue.add("blue");
        queue.add("yellow");
        queue.add("black");

        queue.poll();
        queue.remove();
        queue.offer("red");
    }

    public void deques(){
        Deque deque = new ArrayDeque();
        deque.add("red");
        deque.add("green");
        deque.add("blue");
        deque.add("yellow");
        deque.add("black");

        deque.removeFirst();
        deque.removeLast();
        deque.addFirst("red");
    }

    public void maps(){
        Map<String, String> map = new HashMap<>();
        map.put("red", "red");
        map.put("green", "green");
        map.put("blue", "blue");
        map.put("yellow", "yellow");
        map.put("black", "black");

        map.get("red");
        map.get("green");
        map.get("blue");
        map.get("yellow");
        map.get("black");

        map.put("red", "new red");
        map.put("green", "new green");
        map.put("blue", "new blue");
        map.put("yellow", "new yellow");
        map.put("black", "new black");

        map.remove("red");
        map.remove("green");

        map.entrySet().forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));

        System.out.println(map);
    }

    public void sets(){
    }

}

