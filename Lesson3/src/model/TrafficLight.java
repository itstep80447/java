package model;

public class TrafficLight {
    String light;
    int time;

    public void SetLight(String light, int time) {
        this.light = light;
        this.time = time;
    }

    public String getLight() {
        return this.light;
    }

    public int getTime() {
        return this.time;
    }

    public boolean isGreen() {
        return this.light == "green";
    }

    public boolean isRed() {
        return this.light == "red";
    }

}
