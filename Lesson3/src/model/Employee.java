package model;

public class Employee {
    String name;
    String title;
    double salary;

    public Employee(String name, String title, double salary) {
        this.name = name;
        this.title = title;
        this.salary = salary;
    }

    public double countSalary() {
        return this.salary;
    }

    public double setSalary(double salary) {
        this.salary = salary;
        return salary;
    }
}
