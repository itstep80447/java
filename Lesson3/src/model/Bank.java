package model;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    String name;
    Account[] accounts;

    public Bank(String name) {
        this.name = name;
        this.accounts = new Account[10];
    }

    public void dropAccount(Account account, boolean isOpen) {
        account.DropAccount(isOpen);
    }

    public void createAccount(String name, double balance) {
        Account account = new Account(name, balance);
        this.accounts[this.accounts.length - 1] = account;
    }

    public void createDeposit(Account account, double amount) {
        account.SetDeposit(account.getDeposit() + amount);
    }
}

