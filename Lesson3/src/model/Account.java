package model;

import java.sql.Time;

public class Account {
    String number;
    String name;
    double balance;
    double deposit;
    boolean isOpen;

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
        return this.balance;
    }

    public double getDeposit() {
        return this.deposit;
    }

    public double GetBalanceAndDeposit() {
        return this.balance + this.deposit;
    }

    public void SetBalance(double balance) {
        this.balance = balance;
    }

    public void SetDeposit(double deposit) {
        this.deposit += deposit;
    }

    public void DropAccount(boolean isOpen) {
        this.isOpen = isOpen;
    }


}