import module.Person;
import module.Vector;

import java.io.IOException;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws IOException {
//        // task 1
//        for (int i = 1; i <= 10; i++) {
//            System.out.println(i);
//        }
//
//        // task 2
//        int sum = 0;
//        for (int i = 1; i <= 100; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            sum += i;
//        }
//        System.out.println(sum);
//
//        // task 3 find factorial
//        int result = 1;
//        for (int i = 1; i <= 5; i++) {
//            result *= i;
//        }
//        System.out.println(result);
        Person person = new Person("John", 25);
        System.out.println(person.setName("Jane"));

        Vector vector = new Vector(1, 2, 3);
        Vector vector2 = new Vector(4, 5, 6);
        System.out.println(vector.sum(vector2));
        System.out.println(vector.subtract(vector2));
        System.out.println(vector.multiply(vector2));
        System.out.println(vector.divide(vector2));
        System.out.println(vector.getAngle(vector2));
        System.out.println(vector.getScalarProduct(vector2));
        System.out.println(vector.getRandomVectors(2));
    }
}