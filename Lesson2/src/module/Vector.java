package module;

import java.util.ArrayList;

public class Vector{
        private int x;
        private int y;
        private int z;

        public Vector(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getZ() {
            return z;
        }

        public String toString() {
            return "Vector: (" + "x: " + x + ", y: " + y + ", z: " + z + ")";
        }

        public void setX(int x) {
            this.x = x;
        }

        public void setY(int y) {
            this.y = y;
        }

        public void setZ(int z) {
            this.z = z;
        }

        public Vector sum(Vector vector) {
            return new Vector(x + vector.getX(), y + vector.getY(), z + vector.getZ());
        }
         public Vector subtract(Vector vector) {
            return new Vector(x - vector.getX(), y - vector.getY(), z - vector.getZ());
        }
        public Vector multiply(Vector vector) {
            return new Vector(y * vector.getZ() - z * vector.getY(), z * vector.getX() - x * vector.getZ(), x * vector.getY() - y * vector.getX());
        }
        public Vector divide(Vector vector) {
            return new Vector(x / vector.getX(), y / vector.getY(), z / vector.getZ());
        }

        public ArrayList<Vector> getRandomVectors(int count) {
            ArrayList<Vector> vectors = new ArrayList<>();
            for (int i = 0; i < count; i++) {
                vectors.add(new Vector((int)Math.random() * 100, (int)Math.random() * 100, (int)Math.random() * 100));
            }
            return vectors;
        }

        public double getSqrt(Vector vector) {
            return Math.sqrt(vector.getX() * vector.getX() + vector.getY() * vector.getY() + vector.getZ() * vector.getZ());
        }

        public double getDistance(Vector vector) {
            return Math.sqrt(Math.pow(vector.getX() - x, 2) + Math.pow(vector.getY() - y, 2) + Math.pow(vector.getZ() - z, 2));
        }

        public int getAngle(Vector vector) {
            return (int) Math.toDegrees(Math.atan2(vector.getY(), vector.getX()));
        }

        public int getScalarProduct(Vector vector) {
            return x * vector.getX() + y * vector.getY() + z * vector.getZ();
        }
}
