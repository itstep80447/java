-- Create the desktop_computers table
CREATE TABLE desktop_computers (
    id SERIAL PRIMARY KEY,
    brand VARCHAR(255) NOT NULL,
    model VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    
    cpu_brand VARCHAR(255),
    cpu_model VARCHAR(255),
    cpu_cores INT,
    cpu_threads INT,
    cpu_clock_speed DECIMAL(4, 2),
    
    ram_size INT,
    ram_type VARCHAR(255),
    ram_speed INT,
    
    storage_type VARCHAR(255),
    storage_size INT,
    
    gpu_brand VARCHAR(255),
    gpu_model VARCHAR(255),
    gpu_memory_size INT,
    
    os VARCHAR(255),
    
    motherboard_brand VARCHAR(255),
    motherboard_model VARCHAR(255),
    
    psu_wattage INT,
    
    case_type VARCHAR(255),
    case_color VARCHAR(255),
    
    wifi BOOLEAN,
    bluetooth BOOLEAN,
    usb_ports INT
);

SELECT * FROM desktop_computers;