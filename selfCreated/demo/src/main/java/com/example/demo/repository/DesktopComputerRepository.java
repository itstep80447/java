package com.example.demo.repository;

import com.example.demo.model.DesktopComputer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesktopComputerRepository extends JpaRepository<DesktopComputer, Long> {
}