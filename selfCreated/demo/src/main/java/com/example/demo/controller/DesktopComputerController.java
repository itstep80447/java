package com.example.demo.controller;

import com.example.demo.model.DesktopComputer;
import com.example.demo.service.DesktopComputerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/computers")
public class DesktopComputerController {
    @Autowired
    private DesktopComputerService service;

    @GetMapping
    public List<DesktopComputer> getAllComputers() {
        return service.getAllComputers();
    }

    @GetMapping("/{id}")
    public DesktopComputer getComputerById(@PathVariable Long id) {
        return service.getComputerById(id);
    }

    @PostMapping
    public DesktopComputer saveComputer(@RequestBody DesktopComputer computer) {
        return service.saveComputer(computer);
    }

    @DeleteMapping("/{id}")
    public void deleteComputer(@PathVariable Long id) {
        service.deleteComputer(id);
    }
}
