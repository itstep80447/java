package com.example.demo.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "desktop_computers")
@Data
public class DesktopComputer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String brand;
    private String model;
    private double price;

    @Column(name = "cpu_brand", nullable = false)
    private String cpuBrand;
    @Column(name = "cpu_model", nullable = false)
    private String cpuModel;
    @Column(name = "cpu_cores", nullable = false)
    private int cpuCores;
    @Column(name = "cpu_threads", nullable = false)
    private int cpuThreads;
    @Column(name = "cpu_clock_speed", nullable = false)
    private double cpuClockSpeed;

    @Column(name = "ram_size", nullable = false)
    private int ramSize;
    @Column(name = "ram_type", nullable = false)
    private String ramType;
    @Column(name = "ram_speed", nullable = false)
    private int ramSpeed;

    @Column(name = "storage_type", nullable = false)
    private String storageType;
    @Column(name = "storage_size", nullable = false)
    private int storageSize;

    @Column(name = "gpu_brand", nullable = false)
    private String gpuBrand;
    @Column(name = "gpu_model", nullable = false)
    private String gpuModel;
    @Column(name = "gpu_memory_size", nullable = false)
    private int gpuMemorySize;

    private String os;

    @Column(name = "motherboard_brand", nullable = false)
    private String motherboardBrand;
    @Column(name = "motherboard_model", nullable = false)
    private String motherboardModel;

    @Column(name = "psu_wattage", nullable = false)
    private int psuWattage;

    @Column(name = "case_type", nullable = false)
    private String caseType;
    @Column(name = "case_color", nullable = false)
    private String caseColor;

    private boolean wifi;
    private boolean bluetooth;
    @Column(name = "usb_ports", nullable = false)
    private int usbPorts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    // Getters and setters omitted for brevity
}
