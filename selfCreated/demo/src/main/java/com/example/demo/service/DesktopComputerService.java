package com.example.demo.service;

import com.example.demo.model.DesktopComputer;
import com.example.demo.repository.DesktopComputerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesktopComputerService {
    @Autowired
    private DesktopComputerRepository repository;

    public List<DesktopComputer> getAllComputers() {
        return repository.findAll();
    }

    public DesktopComputer getComputerById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public DesktopComputer saveComputer(DesktopComputer computer) {
        return repository.save(computer);
    }

    public void deleteComputer(Long id) {
        repository.deleteById(id);
    }
}