# GET /api/computers - Retrieve all desktop computers
curl -X GET "http://localhost:8080/api/computers" -H "Content-Type: application/json"

# GET /api/computers/{id} - Retrieve a desktop computer by its ID
curl -X GET "http://localhost:8080/api/computers/{id}" -H "Content-Type: application/json"

# POST /api/computers - Create a new desktop computer
curl -X POST "http://localhost:8080/api/computers" -H "Content-Type: application/json" -d '{
  "brand": "Dell",
  "model": "Inspiron 5000",
  "price": 799.99,
  "cpuBrand": "Intel",
  "cpuModel": "Core i7-9700K",
  "cpuCores": 8,
  "cpuThreads": 16,
  "cpuClockSpeed": 3.6,
  "ramSize": 16,
  "ramType": "DDR4",
  "ramSpeed": 2666,
  "storageType": "SSD",
  "storageSize": 512,
  "gpuBrand": "NVIDIA",
  "gpuModel": "GeForce RTX 2060",
  "gpuMemorySize": 6,
  "os": "Windows 10",
  "motherboardBrand": "ASUS",
  "motherboardModel": "Prime B360M-A",
  "psuWattage": 500,
  "caseType": "ATX",
  "caseColor": "Black",
  "wifi": true,
  "bluetooth": true,
  "usbPorts": 6
}'

# DELETE /api/computers/{id} - Delete a desktop computer by its ID
curl -X DELETE "http://localhost:8080/api/computers/{id}" -H "Content-Type: application/json"
