package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

import java.util.Stack;


@RestController
@RequestMapping("/v1")
public class CustomCalculator {
    @GetMapping("/calculate")
    public int CustomCalculator(@RequestBody String expression) {
        Stack<Integer> stack = new Stack<>();
        char[] chars = expression.toCharArray();
        int num = 0;
        char sign = '+';

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];

            if (Character.isDigit(c)) {
                num = num * 10 + (c - '0');
            }
            if ((!Character.isDigit(c) && c != ' ') || i == chars.length - 1) {
                if (sign == '+') {
                    stack.push(num);
                } else if (sign == '-') {
                    stack.push(-num);
                } else if (sign == '*') {
                    stack.push(stack.pop() * num);
                } else if (sign == '/') {
                    stack.push(stack.pop() / num);
                }
                sign = c;
                num = 0;
            }
        }

        int result = 0;
        while (!stack.isEmpty()) {
            result += stack.pop();
        }
        return result;
    }
}
