package com.example.demo.controller;

import jdk.jfr.DataAmount;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

@RestController
@RequestMapping("/library")
public class LibruaryController {
    ArrayList<Book> books = new ArrayList<>();

    @GetMapping("/books")
    public String books(){
        return "Books:\n" + books.toString();
    }

    @PostMapping("/books")
    public String add(@RequestBody Book book){
        try{
            if(books.stream().anyMatch(b -> Objects.equals(b.id, book.id))){
                throw new Exception("Book already exists");
            }
            books.add(book);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return "Success";
    }

    @GetMapping("/books/{bookId}")
    public String getBook(@PathVariable int bookId){
        try{
            for(Book book : books){
                if(Objects.equals(book.id, bookId)){
                    return book.toString();
                }
            }
            throw new Exception("Book not found");
        }catch(Exception e){
            e.printStackTrace();
        }
        return "Book not found";
    }

    @PutMapping("/books/{bookId}")
    public String updateBook(@RequestBody Book book){
        try{
            for(Book book1 : books){
                if(Objects.equals(book1.id, book.id)){
                    book1.name = book.name;
                    book1.author = book.author;
                    book1.created = book.created;
                    return "Success";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Book not found";
    }

    @DeleteMapping("/books/{bookId}")
    public String deleteBook(@PathVariable int bookId){
        try{
            for(Book book : books){
                if(Objects.equals(book.id, bookId)){
                    books.remove(book);
                    return "Success";
                }
            }

            throw new Exception("Book not found");
        }catch(Exception e){
            e.printStackTrace();
        }
        return "Book not found";
    }
}
