package com.example.demo.controller;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data @Getter @Setter
public class Book {
        int id;
        String name;
        String author;
        Date created;

        public String toString(){
            return "Book{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", author='" + author + '\'' +
                    ", created=" + created +
                    '}';
    }
}
