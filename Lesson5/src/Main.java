import model.JThread;
import model.MyThread;

// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
//        Thread t = Thread.currentThread();
////        t.setName("Main");
////        t.getName();
////        t.setPriority(Thread.MAX_PRIORITY); // 1-10
////        t.getPriority();
////        t.isAlive();
////        t.isInterrupted();
////        t.join();
////        t.start();
////        t.sleep(1000);
////        t.stop(); // old name for interrupt
//        System.out.println(t.getName());

//        System.out.println("Main thread started...");
//        new JThread("JThread").start();
//        System.out.println("Main thread finished...");

//        System.out.println("Main thread started...");
//        for(int i=1; i < 6; i++)
//            new JThread("JThread " + i).start();
//        System.out.println("Main thread finished...");

//        System.out.println("Main thread started...");
//        JThread t = new JThread("JThread ");
//        t.start();
//        try{
//            t.join();
//        }
//        catch(InterruptedException e){
//
//            System.out.printf("%s has been interrupted", t.getName());
//        }
//        System.out.println("Main thread finished...");

//        System.out.println("Main thread started...");
//        MyThread myThread = new MyThread();
//        new Thread(myThread,"MyThread").start();
//
//        try{
//            Thread.sleep(1100);
//
//            myThread.disable();
//
//            Thread.sleep(1000);
//        }
//        catch(InterruptedException e){
//            System.out.println("Thread has been interrupted");
//        }
//        System.out.println("Main thread finished...");

        System.out.println("Main thread started...");
        JThread t = new JThread("JThread");
        t.start();
        try{
            Thread.sleep(150);
            t.interrupt();

            Thread.sleep(150);
        }
        catch(InterruptedException e){
            System.out.println("Thread has been interrupted");
        }
        System.out.println("Main thread finished...");
    }
}