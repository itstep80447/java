package com.asirush.bank.service;

import com.asirush.bank.model.Account;
import com.asirush.bank.model.Transaction;
import com.asirush.bank.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountService accountService;

    public Transaction deposit(Long accountId, BigDecimal amount) {
        Account account = accountService.getAccountById(accountId);
        if (account != null) {
            account.setBalance(account.getBalance().add(amount));
            Transaction transaction = new Transaction();
            transaction.setSenderAccount(null);
            transaction.setReceiverAccount(account);
            transaction.setAmount(amount);
            transaction.setTransactionDate(LocalDateTime.now());
            transaction.setTransactionType("DEPOSIT");
            return transactionRepository.save(transaction);
        }
        return null;
    }

    public Transaction withdraw(Long accountId, BigDecimal amount) {
        Account account = accountService.getAccountById(accountId);
        if (account != null && account.getBalance().compareTo(amount) >= 0) {
            account.setBalance(account.getBalance().subtract(amount));
            Transaction transaction = new Transaction();
            transaction.setSenderAccount(account);
            transaction.setReceiverAccount(null);
            transaction.setAmount(amount);
            transaction.setTransactionDate(LocalDateTime.now());
            transaction.setTransactionType("WITHDRAW");
            return transactionRepository.save(transaction);
        }
        return null;
    }

    public Transaction transfer(Long senderAccountId, Long receiverAccountId, BigDecimal amount) {
        Account senderAccount = accountService.getAccountById(senderAccountId);
        Account receiverAccount = accountService.getAccountById(receiverAccountId);
        if (senderAccount != null && receiverAccount != null && senderAccount.getBalance().compareTo(amount) >= 0) {
            senderAccount.setBalance(senderAccount.getBalance().subtract(amount));
            receiverAccount.setBalance(receiverAccount.getBalance().add(amount));
            Transaction transaction = new Transaction();
            transaction.setSenderAccount(senderAccount);
            transaction.setReceiverAccount(receiverAccount);
            transaction.setAmount(amount);
            transaction.setTransactionDate(LocalDateTime.now());
            transaction.setTransactionType("TRANSFER");
            return transactionRepository.save(transaction);
        }
        return null;
    }

    public List<Transaction> getTransactionHistory(Long accountId) {
        return transactionRepository.findBySenderAccountIdOrReceiverAccountId(accountId, accountId);
    }
}

