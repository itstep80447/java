package model;

import java.util.Queue;
import java.util.function.Consumer;

public class MyQueue {

    //4) Изменить класс MyQueue
    //Изменить класс MyQueue из Межпотоковые коммуникации:
    //
    //Вместо int n добавить Queue<T> (MyQueue сделать обобщенным), которая будет содержать объекты создаваемые Producer.
    //Добавьте еще один объект Consumer, который будет запускаться тоже отдельным потоком.
    //Выводите на консоль какой из объектов Consumer обработал объект из очереди.
    //Измените цикл for на бесконечный цикл.

    public static class Producer implements Runnable {
        private Queue<String> queue;

        public Producer(Queue<String> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                queue.add("Item " + i);
            }
        }
    }
}
