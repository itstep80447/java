package model;

public class CharThread implements Runnable {
    private SharedCharacter sharedChar;

    public CharThread(SharedCharacter sharedChar) {
        this.sharedChar = sharedChar;
    }

    @Override
    public void run() {
        sharedChar.printAndIncrement();
    }
}
