package model;

public class SharedCharacter {
    private char currentChar;

    public SharedCharacter(char startChar) {
        this.currentChar = startChar;
    }

    public synchronized void printAndIncrement() {
        for (int i = 0; i < 100; i++) {
            System.out.print(currentChar);
        }
        System.out.println(); // Move to the next line after printing 100 characters
        currentChar++; // Increment the character
    }
}
