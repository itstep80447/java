package model;

public class NewThread extends Thread {
    // Создать класс расширяющий Thread
    //Создать класс NewThread расширяющий Thread.
    //Переопределить метод run(). В цикле for вывести на консоль символ 100 раз.
    //Создать экземпляр класса и запустить новый поток.
    public static void main(String[] args) {
        NewThread newThread = new NewThread();
        newThread.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("1");
        }
    }
}
