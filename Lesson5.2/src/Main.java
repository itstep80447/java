import model.*;

import java.util.function.Consumer;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        // Task 1: Create a new class that extends Thread.
        NewThread newThread = new NewThread();
        newThread.start();

        // Task 2:
        NewRunnable newRunnable = new NewRunnable();
        newRunnable.start();
        NewRunnable newRunnable1 = new NewRunnable();
        newRunnable1.start();
        NewRunnable newRunnable2 = new NewRunnable();
        newRunnable2.start();

        // Task 3:
        SharedCharacter sharedChar = new SharedCharacter('a');

        Thread t1 = new Thread(new CharThread(sharedChar));
        Thread t2 = new Thread(new CharThread(sharedChar));
        Thread t3 = new Thread(new CharThread(sharedChar));

        t1.start();
        t2.start();
        t3.start();

//        // Task 4:
//        MyQueue<String> queue = new MyQueue<String>();
//        MyQueue.Producer producer = new MyQueue.Producer(queue);
//        Consumer consumer = new Consumer(queue);
//
//        Thread producerThread = new Thread(producer);
//        Thread consumerThread = new Thread(consumer);
//
//        producerThread.start();
//        consumerThread.start();
    }
}