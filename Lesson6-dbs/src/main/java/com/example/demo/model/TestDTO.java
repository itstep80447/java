package com.example.demo.model;

import lombok.Data;

import java.util.Date;

@Data
public class TestDTO {
    public String name;
    public Date year;
    public int age;
}