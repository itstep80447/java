package examplePackage;

public class Calculator {
	public float pi = 3.14f;
	
//	default/public/private/protected
	int add(int a, int b) {
		return a+b;
	}
	public int sub(int a, int b) {
		return a-b;
	}
	protected float div(int a, int b) {
		return a/b;
	}
	private int mul(int a, int b) {
		return a*b;
	}
}
