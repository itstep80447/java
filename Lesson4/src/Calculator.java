public class Calculator {
    // Создайте класс-калькулятор, который принимает строку, содержащую математическое выражение в формате "operand1 operator operand2" (например, "5 + 3").
    //Используйте лямбда-выражения для реализации операций (+, -, *, /). Класс должен возвращать результат операции.
    public static double calculate(String expression) {
        if (expression.contains("+")) {
            return add(expression);
        } else if (expression.contains("-")) {
            return subtract(expression);
        } else if (expression.contains("*")) {
            return multiply(expression);
        } else if (expression.contains("/")) {
            return divide(expression);
        }
        return 0;
    }

    private static int add(String expression) {
        expression = expression.replaceAll(" ", "");
        String[] parts = expression.split("\\+");
        int result = Integer.parseInt(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            result += Integer.parseInt(parts[i]);
        }
        return result;
    }
    private static int subtract(String expression) {
        expression = expression.replaceAll(" ", "");
        String[] parts = expression.split("\\-");
        int result = Integer.parseInt(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            result += Integer.parseInt(parts[i]);
        }
        return result;
    }
    private static double multiply(String expression) {
        expression = expression.replaceAll(" ", "");
        String[] parts = expression.split("\\*");
        int result = Integer.parseInt(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            result *= Integer.parseInt(parts[i]);
        }
        return result;
    }
    private static double divide(String expression) {
        expression = expression.replaceAll(" ", "");
        String[] parts = expression.split("/");
        int result = Integer.parseInt(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            result /= Integer.parseInt(parts[i]);
        }
        return result;
    }
}
