public class StringOperating {
    // Манипуляции со строками:
    //Напишите программу, которая принимает строку и преобразует ее следующим образом:
    //каждое слово в строке, начинающееся с гласной буквы, должно быть перевернуто, а каждое слово, начинающееся с согласной буквы, должно быть написано заглавными буквами.
    // Используйте лямбда-выражения для определения гласных и согласных букв.

    public static String changeCase(String str) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (isVowel(c)) {
                result += c;
            } else {
                result += Character.toUpperCase(c);
            }
        }
        return result;
    }

    private static boolean isVowel(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }
}
