import java.util.ArrayList;
import java.util.List;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Devide devide;
        devide = (x) -> x%13==0;
        boolean result = devide.checkDevide(15);
        System.out.println(result);

        Comparable comparable;
        comparable = (a,b)-> a.length()>b.length()?a:b;
        String resultStr = comparable.compareLength("Hello", "World");
        System.out.println(resultStr);

        Discriminant discriminant;
        discriminant = (a,b,c)->b*b-4*a*c;
        double resultDouble = discriminant.getDiscriminant(1,2,3);
        System.out.println(resultDouble);

        ListString listString;
        listString = (list, subString)->{
            ArrayList<String> newList = new ArrayList<>();
            for (String str : list) {
                if (str.contains(subString)) {
                    newList.add(str);
                }
            }
            return newList;
        };
        List<String> resultList = listString.getListString(new ArrayList<>(), "Hello");
        System.out.println(resultList);

        Calculator calculator = new Calculator();
        double resultTwo = calculator.calculate("56 + 3");

        StringOperating stringOperating = new StringOperating();
        String resultStrTwo = stringOperating.changeCase("Hello, World ambarino");
        System.out.println(resultStrTwo);
    }

    interface Devide{
        boolean checkDevide(int x);
    }

    interface Comparable{
        String compareLength(String a, String b);
    }

    interface Discriminant{
        double getDiscriminant(double a, double b, double c);
    }

    interface ListString{
        List<String> getListString(List<String> list, String subString);
    }
}
//    public static void main(String[] args) {
//        Operationable operation;
//        operation = (x,y)->x+y;
//
//        int result = operation.calculate(1,2);
//        System.out.println(result);
//    }
//
//    interface Operationable{
//        int calculate(int x, int y);
//    }
//
//    public void StringTest(){
//        String str = new String("Hello, World");
//        String str2 = "asd";
//        String str3 = new String(new char[]{'a', 's', 'd'});
//
//        str.compareTo(str2); // return boolean
//        str.toLowerCase().toUpperCase();
//        str.split(","); // return array
//        str.replace("Hello", "Hi");
//        str.replaceAll("Hello", "Hi");
//        str.trim();
//        str.substring(0, 3); // return string
//        str.length();
//        str.charAt(0); // return char
//        str.indexOf("Hello"); // return int
//        str.lastIndexOf("Hello");
//        str.contains("Hello");
//        str.startsWith("Hello");
//        str.endsWith("Hello");
//    }
