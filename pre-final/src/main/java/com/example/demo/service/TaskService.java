package com.example.demo.service;

import com.example.demo.model.Task;
import com.example.demo.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TaskService {

    @Autowired
    private TaskRepository repository;

    public List<Task> getAllTasks() {
        return repository.findAll();
    }

    public Task getTaskById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public Task createTask(Task task) {
        task.setCreatedDate(LocalDateTime.now());
        return repository.save(task);
    }

    public Task updateTask(Long id, Task updatedTask) {
        return repository.findById(id)
                .map(task -> {
                    task.setTitle(updatedTask.getTitle());
                    task.setDescription(updatedTask.getDescription());
                    task.setStatus(updatedTask.getStatus());
                    if (updatedTask.getStatus().equals("completed")) {
                        task.setCompletedDate(LocalDateTime.now());
                    }
                    return repository.save(task);
                })
                .orElse(null);
    }

    public void deleteTask(Long id) {
        repository.deleteById(id);
    }

    public List<Task> getTasksByStatus(String status) {
        return repository.findByStatus(status);
    }
}